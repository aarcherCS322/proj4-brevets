"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

table = [(1000,28,11.428),(600,30,15),(400,32,15),(200,34,15),(0,34,15)]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    hrs = 0

    for i in range(5):
      if control_dist_km >= table[i][0]:
          dist = control_dist_km - table[i][0] 
          hrs += (dist / table[i-1][1])
          control_dist_km -= dist         

    brevet_start_time = arrow.get(brevet_start_time)
    mins = 60*(round(hrs))

    opening_time = brevet_start_time.shift(hours=round(hrs),minutes=mins)

    return opening_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    hrs = 0

    for i in range(5):
      if control_dist_km >= table[i][0]:
          dist = control_dist_km - table[i][0]
          hrs += (dist / table[i-1][2])
          control_dist_km -= dist

    brevet_start_time = arrow.get(brevet_start_time)
    mins = 60*(round(hrs))
    closing_time = brevet_start_time.shift(hours=round(hrs),minutes=mins)

    return closing_time.isoformat()
