Author: Alex Archer

An implementation of flask, ajax, docker and some RUSA rules.

The goal is to reproduce https://rusa.org/octime_acp.html using ajax and flask. 
The logic for the algorithim is described loosely https://rusa.org/pages/acp-brevet-control-times-calculator
There are a number of edge cases and boundaries that are not accounted for...Especially regarding sanitizing
the incoming data. We have to assume valid input and we do not prompt for corrections. 

To Run:
docker build -t name . 
docker run -t -p 5000:5000 name
open your browser to localhost:5000
try it out!
(Find out it doesn't work)


Grader Note:
My acp_times logic is not working. Without that my test cases will not work, but the test cases are present anyway.


